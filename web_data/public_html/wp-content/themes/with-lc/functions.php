<?php

function re_register_post_tag_taxonomy() {
global $wp_rewrite;
$rewrite = array(
'slug' => get_option('tag_base') ? get_option('tag_base') : 'tag',
'with_front' => ! get_option('tag_base') || $wp_rewrite->using_index_permalinks(),
'ep_mask' => EP_TAGS,
);
 
$labels = array(
'name' => _x( 'Tags', 'taxonomy general name' ),
'singular_name' => _x( 'Tag', 'taxonomy singular name' ),
'search_items' => __( 'Search Tags' ),
'popular_items' => __( 'Popular Tags' ),
'all_items' => __( 'All Tags' ),
'parent_item' => null,
'parent_item_colon' => null,
'edit_item' => __( 'Edit Tag' ),
'view_item' => __( 'View Tag' ),
'update_item' => __( 'Update Tag' ),
'add_new_item' => __( 'Add New Tag' ),
'new_item_name' => __( 'New Tag Name' ),
'separate_items_with_commas' => __( 'Separate tags with commas' ),
'add_or_remove_items' => __( 'Add or remove tags' ),
'choose_from_most_used' => __( 'Choose from the most used tags' ),
'not_found' => __( 'No tags found.' )
);
 
register_taxonomy( 'post_tag', 'post', array(
'hierarchical' => true,
'query_var' => 'tag',
'rewrite' => $rewrite,
'public' => true,
'show_ui' => true,
'show_admin_column' => true,
'_builtin' => true,
'labels' => $labels
) );
}
add_action( 'init', 're_register_post_tag_taxonomy', 1 );


?>

<?php

// ウィジェット
register_sidebar();

?>



<?php
add_theme_support('post-thumbnails'); 
?>
<?php
//概要（抜粋）の文字数調整
function my_excerpt_length($length) {
	return 150;
}
add_filter('excerpt_length', 'my_excerpt_length');

?>
<?php
function new_excerpt_more($more) {
	return '...'; 
}
add_filter('excerpt_more', 'new_excerpt_more');

?>
<?php
if(current_user_can('subscriber')){
	add_action( 'admin_init', 'redirect_dashiboard' );
	add_action( 'after_setup_theme', 'subscriber_hide_toolbar' );
}
function redirect_dashiboard() {
 
	if(strpos($_SERVER['SCRIPT_NAME'], 'wp-admin') !==false ){
		wp_redirect( home_url() );
		exit();
	}
}
function subscriber_hide_toolbar() {
	show_admin_bar( false );
}
?>

<?php
if(current_user_can('contributor')){
	add_action( 'admin_init', 'contributor_redirect_dashiboard' );
	add_action( 'after_setup_theme', 'contributor_hide_toolbar' );
}
function contributor_redirect_dashiboard() {
 
	if(strpos($_SERVER['SCRIPT_NAME'], 'wp-admin') !==false ){
		wp_redirect( home_url() );
		exit();
	}
}
function contributor_hide_toolbar() {
	show_admin_bar( false );
}
?>