<?php
/*
Template Name: Contact
*/
?>
<?php
/**
 * The template for displaying pages
 *
 * This is the template that displays all pages by default.
 * Please note that this is the WordPress construct of pages and that
 * other "pages" on your WordPress site will use a different template.
 *
 * @package WordPress
 * @subpackage Twenty_Fifteen
 * @since Twenty Fifteen 1.0
 */

get_header(); ?>
  <link rel="stylesheet" href="<?php echo home_url( '/' ); ?>css/contact.css" />

                <?php while(have_posts()): the_post(); ?>
                  <?php remove_filter('the_content', 'wpautop'); ?>
                    <?php the_content(); ?>
                    <?php endwhile; ?>

                 
<?php get_footer(); ?>
          
  