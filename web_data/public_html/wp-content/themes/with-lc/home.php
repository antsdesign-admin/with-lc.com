<?php
/**
 * The main template file
 *
 * This is the most generic template file in a WordPress theme
 * and one of the two required files for a theme (the other being style.css).
 * It is used to display a page when nothing more specific matches a query.
 * e.g., it puts together the home page when no home.php file exists.
 *
 * Learn more: {@link https://codex.wordpress.org/Template_Hierarchy}
 *
 * @package WordPress
 * @subpackage Twenty_Fifteen
 * @since Twenty Fifteen 1.0
 */

get_header(); ?>
<link rel="stylesheet" href="<?php echo home_url( '/' ); ?>css/top.css" />
 <div class="main-visual cf">
        <div class="inner">
          <img src="<?php echo home_url( '/' ); ?>img/top/mess.svg" alt="">
        </div>
      </div>
      <div id="info" class="info cf">
        <a href="#info" uk-scroll>
          <div class="info-ttl">information</div>
        </a>
         <?php
                            $paged = (int) get_query_var('paged');
                            $args = array(
                                'posts_per_page' => 1,
                                'paged' => $paged,
                                'orderby' => 'post_date',
                                'order' => 'DESC',
                                'post_type' => 'post',
                                'post_status' => 'publish'
                            );
                            $the_query = new WP_Query($args);
                            if ( $the_query->have_posts() ) :
                            while ( $the_query->have_posts() ) : $the_query->the_post();
                            ?>
                            <?php
$cat = get_the_category();
$cat_name = $cat[0]->cat_name;
$cat_slug  = $cat[0]->category_nicename;
?>
        <ul>
          <li class="data"><?php the_time('Y.n.j'); ?></li>
          <li class="cate"><?php echo $cat_name; ?></li>
          <li class="ttl"><?php the_title(); ?></li>
        </ul>
 <?php endwhile; endif; ?>
      </div>
      <main class="cf">
        <div id="c01" class="cf">
          <div class="ttl">ウィズライフクリエイトが目指すもの</div>
          <div class="line"></div>
          <div class="c-ttl">地域包括ケア</div>
          <div class="cont">地域包括ケアでは、ご利用者様を取り巻く様々なサービスの提供者同士の密な連携がカギとなります。地域包括ケアとは、地域にお住いの方が要介護状態となっても、必要なサービスを必要な分だけ利用でき、地域に永く住み続けることができるよう、医療・保険・在宅サービスが十分に整えられ、それが連携し、一体的、体系的に提供される仕組みであり、弊社ではこのシステムの実現に向けて努めてまいります。 </div>
          <a href="concept.html">
            <div class="btn">詳しく見る</div>
          </a>
        </div>
        <div id="c02" class="shinkyu cf">
          <div class="left">
            <div class="bloc">
              <div class="c-ttl">鍼灸事業 
                <span>Acupuncture</span>
              </div>
              <div class="cont">マッサージ・はり灸・リハビリ等を受けてみたいが、自力で通院が出来ない方（介助が必要な方・寝たきりの方）の為に、ご自宅まで訪問して施術を行うサービスです。健康保険を使って行う際は、尚、ご自宅まで直接お伺いする為「院からご自宅までの距離」により往療料(出張料) 
は異なります。介護保険を利用されている方でも訪問リハビリと併用する事は可能で、介護保険の限度額を気にせずにご利用頂けます。 </div>
              <a class="change" href="http://www.with-sin9.com/" target="_blank">
                <div class="btn02">詳しく見る</div>
              </a>
            </div>
          </div>
          <div class="right">
            <img src="<?php echo home_url( '/' ); ?>img/top/ph01.jpg" alt="">
          </div>
        </div>
        <div id="c03" class="kakgo cf">
          <div class="left">
            <img src="<?php echo home_url( '/' ); ?>img/top/ph02.jpg" alt="">
          </div>
          <div class="right">
            <div class="bloc">
              <div class="c-ttl">介護事業 
                <span>Nursing care</span>
              </div>
              <div class="cont">高齢の方、障がいのある方、生活にお困りの方、どんな方でもご相談ください。「介護施設」「グループホーム」「老人ホーム」とは違い、日常生活を送るうえでの制限はなく、自由な生活がおくれます。入居をご希望される方のニーズをお伺いし、生活全般のサポートをいたします。</div>
              <a class="change" href="service.html#c02">
                <div class="btn02">詳しく見る</div>
              </a>
            </div>
          </div>
        </div>
      </main>

        <?php get_footer(); ?>
     