<?php
register_nav_menus(array(
		'main_navigation' => 'Primary Navigation'
	)
);
?>
<?php remove_filter( 'the_content', 'wpautop' ); ?>
<?php remove_filter( 'the_excerpt', 'wpautop' ); ?>
<?php
/* カスタム投稿タイプの追加 */
add_action( 'init', 'create_post_type' );
function create_post_type() {
    register_post_type( 'recommend', /* post-type */
        array(
            'labels' => array(
            'name' => __( 'オススメ' ),
            'singular_name' => __( 'オススメ' )
        ),
        'public' => true,
        'menu_position' =>5,
        )
    );
}?>


<?php add_theme_support('post-thumbnails'); ?>