<?php
/**
 * The template for displaying the header
 *
 * Displays all of the head element and everything up until the "site-content" div.
 *
 * @package WordPress
 * @subpackage Twenty_Fifteen
 * @since Twenty Fifteen 1.0
 */
?>

<!DOCTYPE html>
<html lang="ja">
  <head>
    <meta charset="UTF-8">
    <title><?php wp_title ( '|', true,'right' ); ?><?php bloginfo('name'); ?></title>

    <meta name="format-detection" content="telephone=no">
    <meta name="viewport" content="width=1110">
    <link rel="stylesheet" href="<?php echo home_url( '/' ); ?>css/reset.css" />
    <link rel="stylesheet" href="<?php echo home_url( '/' ); ?>css/uikit.css" />
    <link rel="stylesheet" href="<?php echo home_url( '/' ); ?>css/common.css" />
    <link href="https://fonts.googleapis.com/earlyaccess/notosansjapanese.css" rel="stylesheet" />
    <script src="<?php echo home_url( '/' ); ?>js/uikit.js"></script>
    <script src="<?php echo home_url( '/' ); ?>js/uikit-icons.js"></script>
    <script src="<?php echo home_url( '/' ); ?>js/jquery.min.js"></script>
      <?php wp_head(); ?>
  </head>
    <body uk-scrollspy="cls: uk-animation-fade; target:.fade; delay: 300; repeat: false">
    <div id="wrapper">
      <header class="cf">
        <div class="inner">
          <h1 class="logo">
            <a href="<?php echo home_url( '/' ); ?>"><img src="<?php echo home_url( '/' ); ?>img/common/logo.svg" width="258" alt=""></a>
          </h1>
          <ul>
            <a href="<?php echo home_url( '/' ); ?>concept.html">
              <li>コンセプト</li>
            </a>
            <a href="<?php echo home_url( '/' ); ?>service.html">
              <li>事業内容</li>
            </a>
            <a href="<?php echo home_url( '/' ); ?>company.html">
              <li>会社情報</li>
            </a>
            <a href="<?php echo home_url( '/' ); ?>recruit.html">
              <li>採用情報</li>
            </a>
            <a href="<?php echo home_url( '/' ); ?>contact/">
              <li class="contact">
                <img src="<?php echo home_url( '/' ); ?>img/common/mail.svg" width="22" alt="">お問い合わせ 
              </li>
            </a>
          </ul>
        </div>
      </header>