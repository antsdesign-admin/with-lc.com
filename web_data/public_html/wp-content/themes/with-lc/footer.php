<?php
/**
 * The template for displaying the footer
 *
 * Contains the closing of the #content div and all content after
 *
 * @package WordPress
 * @subpackage Twenty_Sixteen
 * @since Twenty Sixteen 1.0
 */
?>


   <footer class="cf">
        <div class="inner">
          <div class="top-area cf">
            <div class="info-area">
              <div class="logo">
                <a href="<?php echo home_url( '/' ); ?>"><img src="<?php echo home_url( '/' ); ?>img/common/logo.svg" width="309" alt=""></a>
              </div>
              <address>〒062-0004 北海道札幌市豊平区美園4条2丁目2-25
                <br>TEL：011-252-7133／FAX：011-252-7134<br>
<div class="address1">◆ウィズ鍼灸治療院 <br>
【札幌本院】<br>
〒064-0807 北海道札幌市中央区南7条西12丁目1-1-101<br>
【松戸院】<br>
〒270-2242 千葉県松戸市仲井町1-111-1-106 <br>
（北海道院共通） Tel 011-252-7133<br>
（関東地区） Tel 047-315-6259
</div>
<div class="address2">
◆ウィズの介護事業<br>
【ウィズ介護事業部/ウィズ訪問介護ステーション】<br>
〒062-0004 北海道札幌市豊平区美園4条2丁目2-25<br>
（介護事業共通） Tel 0120-972-325
</div>
             
              </address>
            </div>
            <div class="site-map">
              <ul>
                <li>
                  <a href="<?php echo home_url( '/' ); ?>concept.html">コンセプト</a>
                </li>
                <li>
                  <a href="<?php echo home_url( '/' ); ?>service.html">事業内容</a>
                  <span>
                    <a href="http://www.with-sin9.com/" target="_blank">鍼灸事業</a>
                  </span>
                  <span>
                    <a href="<?php echo home_url( '/' ); ?>service.html#c02">介護事業</a>
                  </span>
                </li>
              </ul>
              <ul>
                <li>
                  <a href="<?php echo home_url( '/' ); ?>company.html">会社概要</a>
                </li>
                <li>
                  <a href="<?php echo home_url( '/' ); ?>recruit.html">採用情報</a>
                </li>
                <li>
                  <a href="<?php echo home_url( '/' ); ?>contact/">お問い合わせ</a>
                </li>
              </ul>
            </div>
          </div>
        </div>
        <div class="copy">
          <div class="inner">
            <span>&#169;COPYRIGHT WithLifeCreat Co.,Ltd. all right reserved.</span>
            <div id="pagetop"><a href="#top" uk-scroll>
              <img src="<?php echo home_url( '/' ); ?>img/common/arrow2.svg" width="50" alt="">TOPへ </a>
            </div>
          </div>
        </div>
      </footer>
    </div>
    
    <?php wp_footer(); ?>
  </body>
</html>